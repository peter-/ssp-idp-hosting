#!/bin/bash

# Require root (unless you modify all paths below to not be absolute)
[[ "$(id -u)" == "0" ]] || { echo "This script must be run as root (or be modified to work on relative paths)"; exit 1; }

# Usage
[[ -z "$1" ]] && { echo "Usage: $0 <fqdn>"; exit 1; }

tenent=$1

# Create vhost stub
echo "<VirtualHost *:443>
  Define tenent $tenent
  Include conf-available/tenent-ssl-vhost.conf
</VirtualHost>" > /etc/apache2/sites-available/$tenent.conf

# Enable vhost
a2ensite $tenent

# Create directories
install -d /var/local/simplesaml/{logs,metadata}/$tenent -o www-data -g www-data -m 0750

# Create SAML key pair and tighten permissions
openssl req -newkey rsa:2048 -new -x509 -days 3652 -nodes \
  -out /var/local/simplesaml/cert/${tenent}-crt.pem \
  -keyout /var/local/simplesaml/cert/${tenent}-key.pem \
  -subj "/CN=$tenent"
chgrp www-data /var/local/simplesaml/cert/${tenent}-key.pem
chmod 0640 /var/local/simplesaml/cert/${tenent}-key.pem

# Create initial IDP metadata; could copy from some template
cp -a /var/local/simplesaml/metadata/idp1.lab.example.org/saml20-idp-hosted.php /var/local/simplesaml/metadata/${tenent}/saml20-idp-hosted.php
echo "Now edit /var/local/simplesaml/metadata/${tenent}/saml20-idp-hosted.php"

# SAML IDP-hosting PoC

Write-up and example configurations for a Proof of Concept to use one install of [SimpleSAMLphp](https://simplesamlphp.org/) as basis for mass-hosting virtual SAML IDPs.
See this [REFEDS Wiki Draft](https://wiki.refeds.org/display/GROUPS/Mass-hosting+virtual+IDPs+with+SimpleSAMLphp) for more.

## Assumptions

* Everything was done on a Debian "Jessie" system, so file system paths and some tools are specific to Debian (but can easily adapted on other platforms).
* All commands are being run as `root` or via `sudo`.
* The example hostnames in this PoC all share a common DNS comain (`lab.example.org`) and a hostname schema (`idp<n>`), but this is only for convenience of the PoC and is *not* a requirement for or limitation of the PoC: In fact IDP host names are fully expected (and supoprted) to differ for each IDP institution.
* SimpleSAMLphp is installed (and shared by all instances) in `/usr/local/src/simplesamlphp-$version`, and is made available to each vhost at the REQUEST_URI `/saml` by a creating symbolic link in the default `DocumentRoot` (`/var/www/saml`) pointing to SimpleSAMLphp's `www` directory. (This might also be done using an Apache httpd `Alias` directive within each vhost definiton, though adjusting one symlink seems more flexible to switch between SimpleSAMLphp versions.)
* This PoC does *not* yet utilise the method of setting `SIMPLESAMLPHP_CONFIG_DIR` separately for each (virtual) IDP, as mentioned as part of the [SimpleSAMLphp installation](https://simplesamlphp.org/docs/stable/simplesamlphp-install#section_6) documentation -- though you probably should!
* In this PoC each vhosts points to its own TLS key and cert files, but those currently are merely symbolic links that reference a single shared key pair (using a wildcard cert, though all cert and key files as distributed here are in fact empty).  This means you can (and probably should, unless you intend to use IP-based vhosting) use separate key pairs for each vhost, using [SNI](https://en.wikipedia.org/wiki/Server_Name_Indication).
* Variable data is being held in `/var/local/simplesamlphp`, wheras the SimpleSAMLphp software is in `/usr/local/...`.
* We identfy each "tenent" (IDP institution) based on the `TENENT` environment variable within the web server (becomes a superglobal variable in PHP) and through IDP-specific inodes in the file system, in this case using the IDP's FQDN as value. If the unique tenent identifier is *not* the FQDN of the IDP changes will need to be made in all files referencing this variable, of course. Always use something that's safe to be used as file or directory name on the platform you're deploying this.
* A simple improvement for the existing use of the single variable might be using several variables instead, e.g.:
  * TENENT_FQDN (e.g. `sso.example.org`)
  * TENENT_SCOPE (e.g. `example.org`)
  * TENENT_ADMIN_EMAIL (e.g. `idp-support@it.example.org`)
  * TENENT_DISPLAYNAME (e.g. `Example University`)
  * etc.


## Instructions

Steps to be done only once:

### Prepare the server
Download and install and integrate all the required software (Apache httpd, PHP, SimpleSAMLphp, openssl, etc.)

### Create base directories for tenent IDPs

```bash
install -d /var/local/simplesaml/{data,logs,temp,cert,metadata,metadata/federation} -o www-data -g www-data -m 750
```

### Add a new IDP

To be done for each new IDP.

The required httpd/vhost configuration is minimal and could be created from Ansible, for example. This PoC contains a very simple shell script to give you an idea what kind of steps will need to be taken, based on the SimpleSAMLphp and Apache httpd configuration used.

```bash
./usr/local/bin/add-idp.sh <fqdn-of-the-idp-to-add>
```

Each IDP's `saml20-idp-hosted.php` *will* need to differ, though. The example script merely copies one of the existing example files to the new location for the new IDP.

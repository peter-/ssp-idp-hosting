<?php
/**
 * SAML 2.0 IdP configuration for SimpleSAMLphp.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-hosted
 */

$metadata['https://'.$_SERVER['TENENT'].'/saml'] = array(
	'host' => $_SERVER['TENENT'],
	'scope' => array('example3.org'),

	// X.509 key and certificate. Relative to the cert directory.
	'privatekey'  => '/var/local/simplesaml/cert/'.$_SERVER['TENENT'].'-key.pem',
	'certificate' => '/var/local/simplesaml/cert/'.$_SERVER['TENENT'].'-crt.pem',

	// Authentication source to use. Must be one that is configured in 'config/authsources.php'.
	'auth' => 'example-userpass',
	'userid.attribute' => 'uid', // default: eduPersonPrincipalName

	// Uncomment the following option to start using SHA-256 for your signatures.
	'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',

	// SAML 2.0 options
	'assertion.encryption' => true,
	'saml20.sign.response' => true,
	'saml20.sign.assertion' => false,
	'sign.logout'          => true,
	'redirect.sign'         => true,
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',

	/* Uncomment the following to use the uri NameFormat on attributes. */
	'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:uri',
	'authproc' => array(
		// https://simplesamlphp.org/docs/stable/core:authproc_scopeattribute
		10 => array(
			'class' => 'core:ScopeAttribute',
			'scopeAttribute' => 'example2.org',
			'sourceAttribute' => 'uid',
			'targetAttribute' => 'eduPersonPrincipalName',
		),
		20 => array(
			'class' => 'core:ScopeAttribute',
			'scopeAttribute' => 'eduPersonPrincipalName',
			'sourceAttribute' => 'eduPersonAffiliation',
			'targetAttribute' => 'eduPersonScopedAffiliation',
		),
		// https://simplesamlphp.org/docs/stable/core:authproc_attributerealm
		30 => array(
			'class' => 'core:AttributeRealm',
			'attributename' => 'schacHomeOrganization',
		),
		/*
		90 => array(
			'class' => 'core:AttributeLimit',
			'displayName',
			'eduPersonPrincipalName', 'eduPersonScopedAffiliation',
			'schacHomeOrganization',
		),
		*/
		// Convert LDAP names to oids.
		100 => array('class' => 'core:AttributeMap', 'name2oid'),
	),
	'OrganizationName' => array(
		'de' => 'Beispiel 3 GmbH',
		'en' => 'Example 3 Ltd.',
	),
	'OrganizationDisplayName' => array(
		'de' => 'Beispiel 3',
		'en' => 'Example 3',
	),
	'OrganizationURL' => array(
		'de' => 'http://www.example3.org/',
		'en' => 'http://www.example3.org/en/',
	),
	  'contacts' =>
  array (
    0 =>
    array (
      'contactType' => 'technical',
      'givenName' => 'Vorname',
      'surName' => 'ZUname',
      'emailAddress' =>
      array (
        0 => 'mailto:tech@example3.org',
      ),
    ),
    1 =>
    array (
      'contactType' => 'administrative',
      'givenName' => 'First',
      'surName' => 'Last',
      'emailAddress' =>
      array (
        0 => 'mailto:admin@example3.org',
      ),
    ),
  ),
);

